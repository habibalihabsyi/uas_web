<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ url('') }}/laravel/vendor/almasaeed2010/adminlte/dist/img/user2-160x160.jpg"
                    class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <li class="menu">
                <a href="{{route('adminlte.student.dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard
                </a>
            </li>
            <li class="menu">
                <a href="{{route('adminlte.student.index')}}"><i class="fa fa-table"></i>Data Mahasiswa
                </a>
            </li>
            <li class="menu">
                <a href="{{ route('adminlte.student.create') }}"><i class="fa fa-edit"></i>Input Data Mahasiswa
                </a>
            </li>
            <li class="menu">
                <a href="{{route('adminlte.student.indexjob')}}"><i class="fa fa-table"></i>Data Pekerjaan Orang Tua
                </a>
            </li>
            <li class="menu">
                <a href="{{ route('adminlte.student.createjob') }}"><i class="fa fa-edit"></i>Input Pekerjaan Ortu
                </a>
            </li>
        </ul>
        </form>
    </section>
    <!-- /.sidebar -->
</aside>
